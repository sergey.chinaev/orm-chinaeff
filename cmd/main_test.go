package main

import (
	"reflect"
	"testing"
)

func TestReflectFile(t *testing.T) {
	fileName := "file_test.go"

	reflectDataList, err := ReflectFile(fileName)
	if err != nil {
		t.Errorf("ReflectFile returned an error: %v", err)
	}

	expectedDataList := []ReflectData{
		{
			StructName: "YourStructName1",
			HasDBTag:   true,
			Fields: []FieldInfo{
				{Name: "Field1", Type: "string"},
				{Name: "Field2", Type: "int"},
			},
			Methods: []MethodInfo{
				{Receiver: "YourStructName1", Name: "Method1"},
				{Receiver: "YourStructName1", Name: "Method2"},
			},
		},
		{
			StructName: "YourStructName2",
			HasDBTag:   false,
			Fields: []FieldInfo{
				{Name: "Field3", Type: "float64"},
			},
			Methods: []MethodInfo{
				{Receiver: "YourStructName2", Name: "Method3"},
			},
		},
	}

	if len(reflectDataList) != len(expectedDataList) {
		t.Errorf("Unexpected number of ReflectData elements. Got %d, expected %d", len(reflectDataList), len(expectedDataList))
	}

	for i, reflectData := range reflectDataList {
		expectedData := expectedDataList[i]

		if reflectData.StructName != expectedData.StructName {
			t.Errorf("Unexpected StructName. Got %s, expected %s", reflectData.StructName, expectedData.StructName)
		}

		if reflectData.HasDBTag != expectedData.HasDBTag {
			t.Errorf("Unexpected HasDBTag value. Got %v, expected %v", reflectData.HasDBTag, expectedData.HasDBTag)
		}

		if !reflect.DeepEqual(reflectData.Fields, expectedData.Fields) {
			t.Errorf("Unexpected Fields. Got %v, expected %v", reflectData.Fields, expectedData.Fields)
		}

		if !reflect.DeepEqual(reflectData.Methods, expectedData.Methods) {
			t.Errorf("Unexpected Methods. Got %v, expected %v", reflectData.Methods, expectedData.Methods)
		}
	}
}

func TestMethodExists(t *testing.T) {
	// Create a slice of test methods
	methods := []MethodInfo{
		{Receiver: "YourStructName", Name: "Method1"},
		{Receiver: "YourStructName", Name: "Method2"},
		{Receiver: "YourStructName", Name: "Method3"},
	}

	exists := MethodExists(methods, "YourStructName", "Method2")
	if !exists {
		t.Errorf("Expected methodExists to return true, but got false")
	}

	exists = MethodExists(methods, "YourStructName", "Method4")
	if exists {
		t.Errorf("Expected methodExists to return false, but got true")
	}

	exists = MethodExists([]MethodInfo{}, "YourStructName", "Method1")
	if exists {
		t.Errorf("Expected methodExists to return false, but got true")
	}

	exists = MethodExists(methods, "AnotherStructName", "Method1")
	if exists {
		t.Errorf("Expected methodExists to return false, but got true")
	}
}
